import {PokemonForm} from "pokenode-ts";

interface ControlPanelItem {
    imageURL: string,
    title: string
}

export const controlPanelItems: Array<ControlPanelItem> = [
    {imageURL: "images/pokeball-icon.svg", title: "Pokémon"},
    {imageURL: "images/cogwheel-icon.svg", title: "Settings"},
];


export interface SelectedPokemonForm {
    indexPokeDex: number,
    indexPokeDexChild: number,
    pokeForm: PokemonForm | null,
    pokemonDisplayName: string,
}

export const typeNameArray = ["normal", "fire", "water", "electric", "grass", "ice",
    "fighting", "poison", "ground", "flying", "psychic", "bug", "rock", "ghost", "dragon", "dark", "steel", "fairy"]


// pokemonTypeMatrix[DefensiveTypeIndex][FromTypeIndex] -> Defensive Effectiveness
export const pokemonTypeMatrix: Array<Array<number>> = [
    [1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1], // Normal
    [1, 0.5, 2, 1, 0.5, 0.5, 1, 1, 2, 1, 1, 0.5, 2, 1, 1, 1, 0.5, 0.5], //Fire
    [1, 0.5, 0.5, 2, 2, 0.5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 1], //Water
    [1, 1, 1, 0.5, 1, 1, 1, 1, 2, 0.5, 1, 1, 1, 1, 1, 1, 0.5, 1], //Electric
    [1, 2, 0.5, 0.5, 0.5, 2, 1, 2, 0.5, 2, 1, 2, 1, 1, 1, 1, 1, 1], //Grass
    [1, 2, 1, 1, 1, 0.5, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1], //Ice
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 0.5, 0.5, 1, 1, 0.5, 1, 2], //Fighting
    [1, 1, 1, 1, 0.5, 1, 0.5, 0.5, 2, 1, 2, 0.5, 1, 1, 1, 1, 1, 0.5], //Poison
    [1, 1, 2, 0, 2, 2, 1, 0.5, 1, 1, 1, 1, 0.5, 1, 1, 1, 1, 1], //Ground
    [1, 1, 1, 2, 0.5, 2, 0.5, 1, 0, 1, 1, 0.5, 2, 1, 1, 1, 1, 1], //Flying
    [1, 1, 1, 1, 1, 1, 0.5, 1, 1, 1, 0.5, 2, 1, 2, 1, 2, 1, 1], //Psychic
    [1, 2, 1, 1, 0.5, 1, 0.5, 1, 0.5, 2, 1, 1, 2, 1, 1, 1, 1, 1], //Bug
    [0.5, 0.5, 2, 1, 2, 1, 2, 0.5, 2, 0.5, 1, 1, 1, 1, 1, 1, 2, 1], //Rock
    [0, 1, 1, 1, 1, 1, 0, 0.5, 1, 1, 1, 0.5, 1, 2, 1, 2, 1, 1], //Ghost
    [1, 0.5, 0.5, 0.5, 0.5, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2], //Dragon
    [1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 0, 2, 1, 0.5, 1, 0.5, 1, 2], //Dark
    [0.5, 2, 1, 1, 0.5, 0.5, 2, 0, 2, 0.5, 0.5, 0.5, 0.5, 1, 0.5, 1, 0.5, 0.5], //Steel
    [1, 1, 1, 1, 1, 1, 0.5, 2, 1, 1, 1, 0.5, 1, 1, 0, 0.5, 2, 1] //Fairy
];


export const pokemonTypeColorCode: { [key: string]: string } = {
    "ghost": "#7D66AE",
    "steel": "#ABA9BE",
    "dragon": "#5559CF",
    "flying": "#94B7F4",
    "water": "#46A9ED",
    "ice": "#94E3F1",
    "grass": "#75CA55",
    "bug": "#A8B736",
    "normal": "#C2B8A0",
    "electric": "#FAD33E",
    "ground": "#C8AB63",
    "rock": "#A8865D",
    "fire": "#EC8C3E",
    "fighting": "#9B4840",
    "dark": "#5E4C4C",
    "psychic": "#F5779E",
    "fairy": "#F9A8EF",
    "poison": "#A15AB1",
}

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;
