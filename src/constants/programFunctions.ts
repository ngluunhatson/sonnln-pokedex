export function standardizeName(dashedName: string) {
    const dashedNameSplitArray = dashedName.split("-");
    let returnName = "";
    dashedNameSplitArray.forEach((dashedNameSplit, index) => {
        returnName += dashedNameSplit[0].toUpperCase() + dashedNameSplit.slice(1);
        if (index < dashedNameSplitArray.length - 1)
            returnName += " ";
    });
    return returnName;
}

export function getGenerationIndex(versionGroupName: string): number {
    switch (versionGroupName) {
        case "red-blue":
        case "yellow":
            return 0;
        case "gold-silver":
        case "crystal":
            return 1;
        case "ruby-sapphire":
        case "emerald":
        case "firered-leafgreen":
        case "colosseum":
        case "xd":
            return 2;
        case "diamond-pearl":
        case "platinum":
        case "heartgold-soulsilver":
            return 3;
        case "black-white":
        case "black-2-white-2":
            return 4;
        case "x-y":
        case "omega-ruby-alpha-sapphire":
            return 5;
        case "sun-moon":
        case "ultra-sun-ultra-moon":
        case "lets-go-pikachu-lets-go-eevee":
            return 6;
        case "sword-shield":
        case "the-isle-of-armor":
        case "the-crown-tundra":
        case "brilliant-diamond-and-shining-pearl":
        case "legends-arceus":
            return 7

        case "scarlet-violet":
        case "the-teal-mask":
        case "the-indigo-disk":
            return 8;

        default:
            return -1;
    }
}


export function calculatePokemonStat(isStatHp: boolean, baseStat: number, evNum: number, ivNum: number, level: number, isHelpfulNature: boolean, isShedinja: boolean): number {
    let stat = Math.floor((2 * baseStat + ivNum + Math.floor(evNum / 4)) * level / 100);

    if (isStatHp) {
        if (isShedinja) {
            stat = 1;
        } else {
            stat += level + 10;
        }
    } else {
        stat += 5
        if (isHelpfulNature) {
            stat *= 1.1;
        } else {
            stat *= 0.9;
        }

        stat = Math.floor(stat);
    }

    return stat;
}
