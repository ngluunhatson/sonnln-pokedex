import React, {lazy, useEffect, useState} from "react";
import {PokemonClient, PokemonForm} from "pokenode-ts";
import axios from "axios";
import {Group, Image, Loader, Stack, Text} from "@mantine/core";
import {SelectedPokemonForm} from "@/constants/programConstants";
import {standardizeName} from "@/constants/programFunctions";
import StringSimilarity from "string-similarity-js";
import ListPokemonSortFilter from "@/components/listPokemonSortFilter";

interface ListPokemonProps {
    handleListPokemonClick: (value: SelectedPokemonForm) => void,
    selectedPokemonForm: SelectedPokemonForm,
    setProgressBarPercent: (value: (((prevState: number) => number) | number)) => void
}

interface ListPokemonAllDataObject {
    pokeDexOfPokeFormArray: Array<PokemonForm[]>,
    mapPokeDexNumToImprovedPokeName: Map<number, string>,
}

export default function ListPokemon(props: ListPokemonProps) {
    const [listPokemonAllDataObject, setListPokemonAllDataObject]
        = useState<ListPokemonAllDataObject | null>(null);

    const [stringToFilter, setStringToFilter] = useState("");
    const [typeStringArray, setTypeStringArray] = useState<string[]>([]);
    const pokemonJSXs: JSX.Element[] = [];

    useEffect(() => {
        const api = new PokemonClient();
        const getPokemonForms = async () => {
            const limit = 500;
            let currentOffSet: number = 0;
            let continueFetching: boolean = true;
            let pokeFormPromise: Promise<{
                pokeForm: PokemonForm,
                pokeName: string
            } | null>[] = [];

            const loadPokemonFormLazy = async (currentOffSet: number) => await api.listPokemonForms(currentOffSet, limit);
            const loadEachPokemonData = async (url: string) => await axios.get(url);
            while (continueFetching) {
                const data = await loadPokemonFormLazy(currentOffSet);
                currentOffSet += data.results.length;
                continueFetching = (data.next !== null);
                data.results.forEach((pokeFormData, index) => {
                    pokeFormPromise.push(
                        loadEachPokemonData(pokeFormData.url).then(axiosResponse => {
                            const pokeForm: PokemonForm = axiosResponse.data;
                            let pokeName = pokeForm.name;
                            const pokemonFormName = String(pokeForm.form_name);
                            if (pokemonFormName.length > 0 && pokemonFormName !== "male" && pokemonFormName !== "confined") {
                                pokeName = pokeName.substring(0, (pokeName.length - pokemonFormName.length - 1));
                            }
                            ["indeedee", "meowstic", "toxtricity", "urshifu", "basculegion"].forEach((faultyName) => {
                                if (pokeName.includes(faultyName)) {
                                    pokeName = faultyName;
                                }
                            });

                            if (!(["zygarde-50-power-construct", "zygarde-10", "greninja-battle-bond"].includes(pokeForm.name)
                                || pokemonFormName.includes("totem")
                                || pokemonFormName.includes("starter")
                                || pokeForm.types[0].type.name === "unknown")
                            ) {
                                return {pokeForm: pokeForm, pokeName: pokeName}

                            }
                            return null;

                        }).catch(error => null)
                    );
                });
                props.setProgressBarPercent((prev) => (prev + (((data.results.length / 1.5) / 1450) * 100)));
            }
            Promise.all(pokeFormPromise).then((promiseResArray) => {
                const tempMapPokeNameToPokeDexNum = new Map<string, number>();
                let curPokeDexNum = 0;
                const tempPokeDex = Array<PokemonForm[]>(promiseResArray.length).fill([]);
                const tempMapPokeDexNumToImprovedPokeName = new Map<number, string>();
                promiseResArray.forEach((promiseRes, index) => {
                    if (promiseRes) {

                        const pokeName = promiseRes.pokeName;
                        const pokeForm = promiseRes.pokeForm;
                        const pokeDexNum = tempMapPokeNameToPokeDexNum.get(pokeName);
                        if (pokeDexNum !== undefined) {
                            if (pokeName !== "spewpa" && pokeName !== "scatterbug"
                                && pokeName !== "koraidon" && pokeName !== "miraidon"
                                && !pokeName.includes("mothim") && !pokeName.includes("alcremie")) {
                                tempPokeDex[pokeDexNum] = [...tempPokeDex[pokeDexNum], pokeForm];
                            }

                        } else {
                            tempPokeDex[curPokeDexNum] = [...tempPokeDex[curPokeDexNum], pokeForm];
                            tempMapPokeNameToPokeDexNum.set(pokeName, curPokeDexNum);
                            tempMapPokeDexNumToImprovedPokeName.set(curPokeDexNum++, standardizeName(pokeName));
                        }

                    }
                })
                setListPokemonAllDataObject({
                    pokeDexOfPokeFormArray: tempPokeDex,
                    mapPokeDexNumToImprovedPokeName: tempMapPokeDexNumToImprovedPokeName
                });
                props.setProgressBarPercent(100);
            }).catch(error => console.log(error));

        };
        getPokemonForms().catch(error => console.log(error));

    }, []);

    if (listPokemonAllDataObject)
        listPokemonAllDataObject.pokeDexOfPokeFormArray.forEach((pokeForms, indexPokeDex) => {
            let onePokeJSX: (JSX.Element | undefined)[] = pokeForms.map((pokeForm, indexPokeDexChild) => {
                let sorted = typeStringArray.length === 0;
                if (!sorted) {
                    let filterAnd = true;
                    typeStringArray.forEach(typeName => {
                        let filterOr = false;
                        pokeForm.types.forEach(pokeFormType => {
                            if (pokeFormType.type.name === typeName)
                                filterOr = true;
                        })
                        if (!filterOr) {
                            filterAnd = false;
                        }
                    });
                    sorted = filterAnd;
                }

                if (sorted) {
                    let imageURL = pokeForm.sprites.front_default;
                    if (!imageURL) {
                        const strArray = pokeForm.pokemon.url.split("/");
                        const pokemonId = strArray[strArray.length - 2];
                        imageURL = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonId}.png`;
                    }
                    const formNameFiltered = pokeForm.form_names.filter(a => a.language.name === "en");
                    const formName = formNameFiltered.length > 0 ? formNameFiltered[0].name : "";
                    let originalPokemonName = listPokemonAllDataObject.mapPokeDexNumToImprovedPokeName.get(indexPokeDex);

                    let displayName = originalPokemonName + (formName !== "" ? ` ${formName}` : "");
                    if (pokeForm.names.length === 0) {
                        if (formName.includes("Mega") || formName === "Ash-Greninja") {
                            displayName = formName;
                        }
                    } else {
                        const tempNameList = pokeForm.names.filter(a => a.language.name === "en");
                        if (tempNameList.length > 0) {
                            displayName = tempNameList[0].name;
                        }
                    }

                    if (indexPokeDexChild > 0 && indexPokeDex === 127) {
                        displayName = originalPokemonName ? originalPokemonName : "";
                        displayName += (" " + standardizeName(pokeForm.form_name));
                    } // Tauros

                    const stringSimilarityCoEfficient = StringSimilarity(displayName, stringToFilter);
                    if (stringToFilter.length < 3 || stringSimilarityCoEfficient > 0.35 || displayName.includes(stringToFilter))
                        return (
                            <PokemonCard
                                key={`pokemon-${indexPokeDex}-${indexPokeDexChild}`}
                                indexPokeDex={indexPokeDex}
                                displayName={displayName} imageURL={imageURL} pokeForm={pokeForm}
                                handleClick={props.handleListPokemonClick}
                                isSelectedPokemonForm={props.selectedPokemonForm.indexPokeDex === indexPokeDex
                                    && props.selectedPokemonForm.indexPokeDexChild === indexPokeDexChild}
                                indexPokeDexChild={indexPokeDexChild}/>
                        );
                }
            });
            
            pokemonJSXs.push(
                <Stack spacing={0} key={`pokemon-${indexPokeDex}`}>
                    {onePokeJSX}
                </Stack>
            );
        });

    return (
        <Stack spacing={0} className={"listPokemon"}>
            <ListPokemonSortFilter setStringToFilter={setStringToFilter} setTypeStringArray={setTypeStringArray}
                                   typeStringArray={typeStringArray}/>
            <Stack spacing={0} className={"listPokemon-list"}>
                {pokemonJSXs}
            </Stack>
        </Stack>
    );
}


interface PokemonCardProps {
    displayName: string,
    indexPokeDex: number,
    indexPokeDexChild: number,
    imageURL: string | null,
    pokeForm: PokemonForm,
    isSelectedPokemonForm: boolean,
    handleClick: (value: SelectedPokemonForm) => void
}

function PokemonCard(props: PokemonCardProps) {
    const handleClick = (indexPokeDex: number, indexPokeDexChild: number) => {
        const tempSelectedPokemonForm: SelectedPokemonForm = {
            indexPokeDex: indexPokeDex,
            pokeForm: props.pokeForm,
            indexPokeDexChild: indexPokeDexChild,
            pokemonDisplayName: props.displayName,
        };
        props.handleClick(tempSelectedPokemonForm);
    };
    return (
        <Group className={"pokemonCard" + (props.isSelectedPokemonForm ? " pokemonCard-selected" : "")}
               onClick={() => handleClick(props.indexPokeDex, props.indexPokeDexChild)}>
            <Image width={100}
                   src={props.imageURL}
                   withPlaceholder
                   placeholder={<Loader variant={"dots"} c={"blue"}/>}
                   alt={props.displayName}
                   imageProps={{loading: "lazy"}}/>
            <Stack className={"pokemonCard-info"}>
                <Text fw={300} c={"gray"}>{String(props.indexPokeDex + 1).padStart(4, "0")}</Text>
                <Stack spacing={5}>
                    <Text fw={"700"} c={"white"}>{props.displayName}</Text>
                    <Group spacing={5}>
                        {props.pokeForm.types.map((typeObject: any, index: number) =>
                            <Image
                                alt={"no_image"}
                                key={`${props.pokeForm.id}-type${index}`}
                                width={20}
                                src={`/images/type_icons/${typeObject.type.name}.svg`}
                                withPlaceholder
                                placeholder={<Image key={`${props.pokeForm.id}-type${index}`}
                                                    width={20}
                                                    src={`/images/type_icons/normal.svg`}
                                                    alt={"no_image"}/>}
                            />
                        )}
                    </Group>
                </Stack>
            </Stack>
        </Group>
    );
}
