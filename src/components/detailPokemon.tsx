import React, {useEffect, useState} from "react";
import {
    pokemonTypeColorCode,
    pokemonTypeMatrix,
    SelectedPokemonForm,
    typeNameArray
} from "@/constants/programConstants";
import {
    ActionIcon,
    Anchor,
    Badge,
    Center,
    Collapse,
    Group,
    Image,
    Loader,
    SimpleGrid,
    Stack,
    Table,
    Tabs,
    Text,
    Tooltip
} from "@mantine/core";
import {
    Ability,
    Move,
    MoveClient,
    Pokemon,
    PokemonAbility,
    PokemonClient,
    PokemonForm,
    PokemonMove,
    PokemonSpecies
} from "pokenode-ts";
import axios from "axios";
import PokeBallLogo from "@/components/pokemonBallLogo";
import {useDisclosure} from "@mantine/hooks";
import {GiAlliedStar} from "react-icons/gi";
import {AiFillCaretDown} from "react-icons/ai";
import {calculatePokemonStat, getGenerationIndex, standardizeName} from "@/constants/programFunctions";
import {FaChalkboardTeacher, FaLevelUpAlt} from "react-icons/fa";
import {BsEggFill} from "react-icons/bs";
import {TiArrowBack} from "react-icons/ti";
import {RiDiscFill} from "react-icons/ri";

interface DetailPokemonProps {
    selectedPokemonForm: SelectedPokemonForm,
    handleBackButtonClick: () => void,
    setFullScreenMode: (value: (((prevState: boolean) => boolean) | boolean)) => void,
    fullScreenMode: boolean
}


interface DetailPokemonAllDataType {
    moveListMap: Map<string, Array<Array<{ learnedAt: number, moveObject: Move }>>>,
    abilityData: (Ability | null)[]
    pokeForm: PokemonForm,
    pokeInfo: Pokemon,
    pokeSpecies: PokemonSpecies,
    typeEffectiveDefensiveMap: Map<number, Array<string>>
}


export default function DetailPokemon(props: DetailPokemonProps) {
    const pokeForm = props.selectedPokemonForm.pokeForm;

    const [detailPokemonAllDataType, setDetailPokemonAllDataType]
        = useState<DetailPokemonAllDataType | null>(null);
    const [pokemonLoaded, setPokemonLoaded] = useState(true);


    const cleanUpFunction = () => {
        setDetailPokemonAllDataType(null);
        setPokemonLoaded(true);
    }

    useEffect(() => {
        const start = new Date().getTime()
        if (pokeForm) {
            setPokemonLoaded(false);
            const tempTypeEffectiveDefensiveMap = new Map<number, Array<string>>();
            tempTypeEffectiveDefensiveMap.set(0, []);
            tempTypeEffectiveDefensiveMap.set(0.25, []);
            tempTypeEffectiveDefensiveMap.set(0.5, []);
            tempTypeEffectiveDefensiveMap.set(1, []);
            tempTypeEffectiveDefensiveMap.set(2, []);
            tempTypeEffectiveDefensiveMap.set(4, []);


            for (let i = 0; i <= 17; i++) {
                let defensiveTypeEV = 1;

                pokeForm.types.forEach(typeObject => {
                    const typeName = typeObject.type.name;
                    const matrixIndex = typeNameArray.indexOf(typeName);
                    defensiveTypeEV *= pokemonTypeMatrix[matrixIndex][i];
                });
                const tempArray = tempTypeEffectiveDefensiveMap.get(defensiveTypeEV);
                const resultTempArray = tempArray ? tempArray : [];
                tempTypeEffectiveDefensiveMap.set(defensiveTypeEV, [...resultTempArray, typeNameArray[i]]);
            }
            const getPokemonData = async () => {
                return await axios.get(pokeForm.pokemon.url);
            };

            const getPokemonSpecies = async (url: string) => {
                return await axios.get(url);
            };

            getPokemonData().then(axiosResponse => {
                const pokeInfoData: Pokemon = axiosResponse.data;
                getPokemonSpecies(pokeInfoData.species.url).then(pokeSpeciesAxiosResponse => {
                    const pokeSpeciesData: PokemonSpecies = pokeSpeciesAxiosResponse.data;
                    const apiMove = new MoveClient();
                    const apiPokemon = new PokemonClient();

                    const movePromises: Promise<{ pokemonMoveObject: PokemonMove, moveData: Move } | null>[]
                        = pokeInfoData.moves.map((pokemonMoveObject) => {
                        const getPokemonMove = async () => await apiMove.getMoveByName(pokemonMoveObject.move.name);
                        return getPokemonMove().then(moveData => {
                            return {pokemonMoveObject: pokemonMoveObject, moveData: moveData}
                        }).catch(error => null);
                    });

                    const abilityPromises: Promise<{
                        pokemonAbilityObject: PokemonAbility,
                        abilityData: Ability
                    } | null>[]
                        = pokeInfoData.abilities.map(pokemonAbilityObject => {
                        const getAbilityData = async () => await apiPokemon.getAbilityByName(pokemonAbilityObject.ability.name);
                        return getAbilityData().then(abilityData => {
                            return {pokemonAbilityObject: pokemonAbilityObject, abilityData: abilityData}
                        }).catch(error => null);
                    })

                    Promise.all(movePromises).then(promiseResArray => {
                        const tempMoveListMap = new Map<string, Array<Array<{
                            learnedAt: number,
                            moveObject: Move
                        }>>>();
                        promiseResArray.forEach(promiseRes => {
                            if (promiseRes) {
                                promiseRes.pokemonMoveObject.version_group_details.forEach((versionGroupDetail) => {
                                    const moveLearnMethod = versionGroupDetail.move_learn_method.name;
                                    if (!tempMoveListMap.has(moveLearnMethod)) {
                                        tempMoveListMap.set(moveLearnMethod, Array(9).fill([]));
                                    }

                                    const versionGroupName = versionGroupDetail.version_group.name;
                                    const generationIndex = getGenerationIndex(versionGroupName);

                                    const tempMoveArray = tempMoveListMap.get(moveLearnMethod);
                                    if (tempMoveArray) {
                                        const isDuplicate
                                            = tempMoveArray[generationIndex].find(e => e.moveObject.name === promiseRes.pokemonMoveObject.move.name);
                                        if (!isDuplicate) {
                                            const setData = {
                                                learnedAt: versionGroupDetail.level_learned_at,
                                                moveObject: promiseRes.moveData
                                            }
                                            tempMoveArray[generationIndex] = [...tempMoveArray[generationIndex], setData];
                                            tempMoveListMap.set(moveLearnMethod, tempMoveArray);
                                        }
                                    }
                                });
                            }
                        });

                        const tempMoveListArray = tempMoveListMap.get("level-up");
                        if (tempMoveListArray) {
                            const tempMoveListArraySorted = tempMoveListArray.map(tempMoveListArraySingleton => {
                                return tempMoveListArraySingleton.sort((a, b) => {
                                    return a.learnedAt - b.learnedAt;
                                });
                            });
                            tempMoveListMap.set("level-up", tempMoveListArraySorted);
                        }


                        Promise.all(abilityPromises).then(abilityResArray => {
                            const tempAbilityArray = Array<Ability | null>(3).fill(null);

                            let normalAbilityIndex = 0;
                            abilityResArray.forEach(abilityRes => {
                                if (abilityRes) {
                                    if (abilityRes.pokemonAbilityObject.is_hidden)
                                        tempAbilityArray[2] = abilityRes.abilityData;
                                    else {
                                        tempAbilityArray[normalAbilityIndex++] = abilityRes.abilityData;
                                    }
                                }
                            });
                            setDetailPokemonAllDataType({
                                pokeForm: pokeForm,
                                abilityData: tempAbilityArray,
                                pokeInfo: pokeInfoData,
                                pokeSpecies: pokeSpeciesData,
                                typeEffectiveDefensiveMap: tempTypeEffectiveDefensiveMap,
                                moveListMap: tempMoveListMap
                            });
                            setPokemonLoaded(true);


                            console.log("------------------------------------------------------------");
                            console.log("pokeInfoData", pokeInfoData);
                            console.log("pokeFormData", pokeForm);
                            console.log("pokeSpeciesData", pokeSpeciesData);
                            console.log("tempTypeEffectiveDefensiveMap", tempTypeEffectiveDefensiveMap);
                            console.log("tempAbilityArray", tempAbilityArray);
                            console.log("tempMoveListMap", tempMoveListMap);
                            console.log("------------------------------------------------------------");
                            const end = new Date().getTime();
                            const timeInSecond = (end - start) / 1000;
                            console.log(`Execution time: ${timeInSecond} seconds`);
                        });


                    }).catch(error => console.log(error));


                }).catch(console.error);
            }).catch(error => console.log(error));
        }
        return cleanUpFunction;
    }, [pokeForm?.order]);


    let mainContent = <></>;
    const loaderJSX = (
        <Center style={{height: "100vh"}}>
            <Stack spacing={20} className={"detailPokemon-loader"}>
                <Text ta="center" fw={700} fz={24}>Loading Pokemon</Text>
                <Center>
                    <Loader variant={"bars"}/>
                </Center>
            </Stack>
        </Center>
    );


    if (detailPokemonAllDataType) {

        const pokeForm = detailPokemonAllDataType.pokeForm;
        const pokeInfo = detailPokemonAllDataType.pokeInfo;
        const typeEffectiveDefensiveMap = detailPokemonAllDataType.typeEffectiveDefensiveMap;
        const pokeSpecies = detailPokemonAllDataType.pokeSpecies;
        const moveListMap = detailPokemonAllDataType.moveListMap

        let imageURL = pokeInfo.sprites.other?.["official-artwork"].front_default;
        if (pokeInfo && pokeInfo.forms.length > 1 && ![665, 664, 869].includes(pokeInfo.id) && pokeForm.sprites.front_default) {
            imageURL = pokeForm.sprites.front_default;
        }

        const pokemonGenusEnglishName = pokeSpecies
            ? pokeSpecies.genera.filter(genusObject => genusObject.language.name === "en")[0].genus
            : "";

        const pokemonFirstTypeName = pokeForm.types[0].type.name

        const pokeTypeText = (
            <Center>
                <Group spacing={8}>
                    {pokeForm.types.map((typeObject, index) => {
                        const typeName = typeObject.type.name;
                        return (
                            <div key={`type-${index}`} className={"detailPokemon-pokemonInfo-pokemonTypeSection"}>
                                <Text fw={700}
                                      c={pokemonTypeColorCode[typeName]}>{standardizeName(typeName)}</Text>
                            </div>
                        );

                    })}
                </Group>
            </Center>
        );

        const pokemonMainColor = pokemonTypeColorCode[pokemonFirstTypeName];

        const getPokemonTypeEffectiveAreaJSX: (() => JSX.Element) = () => {
            const getPokemonTypeChip = (typeName: string, effectiveNum: number, key: string) => {
                let effectiveNumString = String(effectiveNum);
                if (effectiveNum === 0.5) {
                    effectiveNumString = "½";
                }

                if (effectiveNum === 0.25) {
                    effectiveNumString = "¼";
                }

                return (
                    <Center key={key}>
                        <Tooltip withArrow={true} color={pokemonTypeColorCode[typeName]}
                                 label={`${effectiveNum}x ${standardizeName(typeName)} Damage`}>
                            <Group position={"center"} spacing={5} style={{
                                backgroundColor: pokemonTypeColorCode[typeName],
                                borderRadius: 16,
                                padding: 8,
                                height: 45,
                                width: 85,
                                border: `1px solid white`
                            }}>
                                <Image src={`/images/type_icons/${typeName}.svg`} width={20}
                                       alt={`type-${typeName}-image`}/>
                                <Text fw={700} fz={13} c={"white"}>{effectiveNumString}×</Text>
                            </Group>
                        </Tooltip>
                    </Center>
                );
            }

            return (
                <SimpleGrid cols={4}>
                    {[4, 2, 0.5, 0.25, 0].map((effectiveNum: number) => {
                        const returnJSX: JSX.Element[] = [];
                        const tempTypeStringArray = typeEffectiveDefensiveMap.get(effectiveNum);
                        const typeStringArray = tempTypeStringArray ? tempTypeStringArray : []
                        typeStringArray.forEach((typeName: string, index: number) => {
                            returnJSX.push(getPokemonTypeChip(typeName, effectiveNum, `effective-${effectiveNum}-typeChip-${index}`));
                        })
                        return returnJSX;
                    })}
                </SimpleGrid>

            );
        }

        const getPokemonMoveListJSX: (() => JSX.Element) = () => {
            const defaultColorTabIcon = pokemonMainColor;
            const defaultSizeTabIcon = "0.7rem";
            const defaultLearnMethods: { name: string, iconJSX: JSX.Element, value: string }[] = [
                {
                    name: "Level",
                    value: "level-up",
                    iconJSX: <FaLevelUpAlt color={defaultColorTabIcon} size={defaultSizeTabIcon}/>
                },
                {
                    name: "TM/HM",
                    value: "machine",
                    iconJSX: <RiDiscFill color={defaultColorTabIcon} size={defaultSizeTabIcon}/>
                },
                {
                    name: "Tutor",
                    value: "tutor",
                    iconJSX: <FaChalkboardTeacher color={defaultColorTabIcon} size={defaultSizeTabIcon}/>
                },
                {
                    name: "Egg",
                    value: "egg",
                    iconJSX: <BsEggFill color={defaultColorTabIcon} size={defaultSizeTabIcon}/>
                }
            ];

            let mainTabLength = 0;
            const mainTabJSX = defaultLearnMethods.map((defaultLearnMethod, index) => {
                if (moveListMap.has(defaultLearnMethod.value)) {
                    mainTabLength++;
                    return <Tabs.Tab value={`${defaultLearnMethod.value}`}
                                     icon={defaultLearnMethod.iconJSX}
                                     key={`tab-learn-method-${index}`}>{defaultLearnMethod.name}</Tabs.Tab>
                }
            })

            return (
                <Tabs color={"cyan"} variant="pills" defaultValue={defaultLearnMethods[0].value}>
                    <Tabs.List position={"center"}>
                        <SimpleGrid cols={mainTabLength}>
                            {mainTabJSX}
                        </SimpleGrid>
                    </Tabs.List>
                    {
                        defaultLearnMethods.map((defaultLearnMethod, index) => {
                            const moveListByGeneration = moveListMap.get(defaultLearnMethod.value);
                            if (moveListByGeneration) {
                                let defaultValueTab = ""
                                const moveListTableJSX = moveListByGeneration.map((moveList, generationIndex) => {
                                    if (moveList.length > 0) {
                                        if (defaultValueTab === "") {
                                            defaultValueTab = `${defaultLearnMethod.value}-gen-${generationIndex}`;
                                        }

                                        const isLevelUpMethod = defaultLearnMethod.value === "level-up";
                                        let tableHeaders = (
                                            <tr>
                                                {isLevelUpMethod ? <th>Level</th> : <th>Type</th>}
                                                <th>Name</th>
                                                <th>Category</th>
                                            </tr>
                                        );


                                        const tableRows = moveList.map((move, moveIndex) => {
                                            const moveTypeName = move.moveObject.type.name
                                            const standardizedMoveName = standardizeName(move.moveObject.name);
                                            const moveWikiUrl = "https://bulbapedia.bulbagarden.net/wiki/"
                                                + standardizedMoveName.replace(" ", "_") + "_(move)";

                                            return (
                                                <tr key={`tab-panel-learn-method-${index}-gen-${generationIndex}-move-${moveIndex}`}>
                                                    <td>
                                                        {isLevelUpMethod
                                                            ? move.learnedAt === 0 ? "On Evolve" : move.learnedAt
                                                            : <Text
                                                                c={pokemonTypeColorCode[moveTypeName]}>
                                                                {standardizeName(moveTypeName)}
                                                            </Text>}
                                                    </td>
                                                    <td>
                                                        <Anchor href={moveWikiUrl} target={"_blank"}>
                                                            <Group spacing={5}>
                                                                <Text fw={700}
                                                                      c={pokemonTypeColorCode[moveTypeName]}>
                                                                    {standardizedMoveName}
                                                                </Text>
                                                                <Image width={20} alt={""}
                                                                       src={`/images/type_icons/${moveTypeName}.svg`}/>
                                                            </Group>
                                                        </Anchor>
                                                    </td>
                                                    <td>
                                                        <Center>
                                                            <Text ta={"center"}
                                                                  style={{borderRadius: 10, padding: 5, width: 70}}
                                                                  fw={700}
                                                                  className={`detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-moveListTable-statusText-${move.moveObject.damage_class?.name}`}>
                                                                {standardizeName(move.moveObject.damage_class ? move.moveObject.damage_class.name : "")}
                                                            </Text>
                                                        </Center>
                                                    </td>
                                                </tr>
                                            )
                                        });

                                        return (
                                            <Tabs.Panel pl={"xs"}
                                                        key={`tab-panel-learn-method-${index}-gen-${generationIndex}`}
                                                        value={`${defaultLearnMethod.value}-gen-${generationIndex}`}>
                                                <Stack spacing={10}>
                                                    <Text ta={"center"} fz={14} fw={700} bg={"white"}
                                                          color={pokemonMainColor}>Move
                                                        List By {defaultLearnMethod.name} -
                                                        Gen {generationIndex + 1}</Text>
                                                    <div
                                                        className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-moveListTable"}>
                                                        <Table withBorder
                                                               highlightOnHover
                                                               withColumnBorders>
                                                            <thead>{tableHeaders}</thead>
                                                            <tbody>{tableRows}</tbody>
                                                        </Table>
                                                    </div>
                                                </Stack>
                                            </Tabs.Panel>
                                        )
                                    }

                                });
                                return (
                                    <Tabs.Panel key={`tab-panel-learn-method-${index}`}
                                                value={`${defaultLearnMethod.value}`}
                                                pt="xs">
                                        <Tabs color={"cyan"} variant="pills" defaultValue={defaultValueTab}
                                              orientation={"vertical"}>
                                            <Tabs.List position={"left"}>
                                                {
                                                    moveListByGeneration.map((moveList, generationIndex) => {
                                                        if (moveList.length > 0)
                                                            return (
                                                                <Tabs.Tab
                                                                    key={`tab-learn-method-${index}-gen-${generationIndex}`}
                                                                    value={`${defaultLearnMethod.value}-gen-${generationIndex}`}>
                                                                    Gen {generationIndex + 1}
                                                                </Tabs.Tab>
                                                            )
                                                    })
                                                }
                                            </Tabs.List>
                                            {moveListTableJSX}

                                        </Tabs>
                                    </Tabs.Panel>
                                )
                            }
                        })
                    }

                </Tabs>
            )
        }

        const getPokemonBaseStatJSX: (() => JSX.Element) = () => {
            let totalStat = 0;

            return (
                <Stack spacing={10}>
                    {
                        pokeInfo.stats.map((pokemonStat, index) => {
                            let statName = standardizeName(pokemonStat.stat.name);
                            let isStatHp = false;
                            let colorCode = "";
                            let barColorCode = "";
                            totalStat += pokemonStat.base_stat;

                            switch (pokemonStat.stat.name) {
                                case "hp":
                                    statName = "HP";
                                    isStatHp = true;
                                    colorCode = "hsl(0,100%, 80%)";
                                    barColorCode = "#ff0000"
                                    break;

                                case "attack":
                                    statName = "ATK";
                                    colorCode = "hsl(25,86%, 80%)";
                                    barColorCode = "#f08030"
                                    break;

                                case "defense":
                                    statName = "DEF";
                                    colorCode = "hsl(48,93%,78%)"
                                    barColorCode = "#f8d030";
                                    break;

                                case "special-attack":
                                    statName = "Sp.ATK";
                                    colorCode = "hsl(222,81%,90%)"
                                    barColorCode = "#6890f0";
                                    break;
                                case "special-defense":
                                    statName = "Sp.DEF";
                                    colorCode = "hsl(100,52%,84%)";
                                    barColorCode = "#78c850"
                                    break;

                                case "speed":
                                    statName = "SPD";
                                    colorCode = "hsl(342,91%,84%)";
                                    barColorCode = "#f85888";
                                    break;
                                default:
                                    break;
                            }

                            const lowestStatAt50
                                = calculatePokemonStat(isStatHp, pokemonStat.base_stat, 0, 0, 50, false, pokeInfo.name === "shedinja");

                            const lowestStatAt100
                                = calculatePokemonStat(isStatHp, pokemonStat.base_stat, 0, 0, 100, false, pokeInfo.name === "shedinja");

                            const highestStatAt50
                                = calculatePokemonStat(isStatHp, pokemonStat.base_stat, 252, 31, 50, true, pokeInfo.name === "shedinja");

                            const highestStatAt100
                                = calculatePokemonStat(isStatHp, pokemonStat.base_stat, 252, 31, 100, true, pokeInfo.name === "shedinja");

                            return (
                                <Tooltip color={colorCode} key={index} label={(
                                    <Stack spacing={2}>
                                        <Text fw={700} color={"black"}>{statName}: {pokemonStat.base_stat}</Text>
                                        <Group spacing={2}>
                                            <Text fw={700} color={"black"}>
                                                Lvl.50 Stat: {lowestStatAt50} - {highestStatAt50}
                                            </Text>
                                            <Text color={"red"} fw={700}>*</Text>
                                        </Group>
                                        <Group spacing={2}>
                                            <Text fw={700} color={"black"}>
                                                Lvl.100 Stat: {lowestStatAt100} - {highestStatAt100}
                                            </Text>
                                            <Text color={"red"} fw={700}>*</Text>
                                        </Group>
                                    </Stack>
                                )}>
                                    <Group style={{width: "100%"}} spacing={10}>
                                        <div
                                            style={{
                                                width: "18%",
                                                padding: 5,
                                                backgroundColor: barColorCode,
                                                borderRadius: 10
                                            }}>
                                            <Text ta={"center"} fw={700} c={"white"}
                                                  className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-statText"}>{statName}</Text>
                                        </div>
                                        <div style={{width: "70%", backgroundColor: colorCode, borderRadius: 10}}>
                                            <div style={{
                                                width: `${(pokemonStat.base_stat / 180 * 100)}%`,
                                                backgroundColor: barColorCode,
                                                height: 30,
                                                borderRadius: 10,
                                                border: `1px solid ${barColorCode}`
                                            }}></div>
                                        </div>
                                        <div style={{width: "6%"}}>
                                            <Text ta={"center"} fw={700} c={barColorCode}>{pokemonStat.base_stat}</Text>
                                        </div>
                                    </Group>
                                </Tooltip>
                            )
                        })
                    }

                    <Group style={{width: "100%"}} spacing={10}>

                        <div
                            style={{
                                width: "18%",
                                padding: 5,
                                backgroundColor: "var(--accent-color-7)",
                                borderRadius: 10
                            }}>
                            <Text ta={"center"} fw={700} c={"white"}
                                  className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-statText"}>Total: {totalStat}</Text>
                        </div>
                        <div
                            style={{
                                width: "18%",
                                padding: 5,
                                backgroundColor: "var(--accent-color-7)",
                                borderRadius: 10
                            }}>
                            <Text ta={"center"} fw={700} c={"white"}
                                  className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-statText"}>Avg: {Math.floor(totalStat / 6)}</Text>
                        </div>
                        <Anchor
                            href={`https://bulbapedia.bulbagarden.net/wiki/Category:Pok%C3%A9mon_with_a_base_stat_total_of_${totalStat}`}
                            target={"_blank"} style={{width: "55%"}}>

                            <Text fz={13} fw={700}>Other Pokémon with base stat of {totalStat}</Text>
                        </Anchor>


                    </Group>
                    <Stack spacing={2}>
                        <Group spacing={5}>
                            <Text fw={700} c={"red"}>*</Text>
                            <Text fw={700}
                                  className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-statAsteriskText"}>Minimum
                                stats are calculated with 0 EVs, IVs of 0, and (if
                                applicable) a hindering nature.</Text>
                        </Group>

                        <Group spacing={10}>
                            <div></div>
                            <Text fw={700}
                                  className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-statAsteriskText"}>Maximum
                                stats are calculated with 252 EVs, IVs of 31, and (if
                                applicable) a helpful nature.</Text>
                        </Group>
                    </Stack>
                </Stack>
            );
        }

        const getPokemonAbilityJSX: (() => JSX.Element) = () => {

            const firstAbility = detailPokemonAllDataType.abilityData[0];
            const secondAbility = detailPokemonAllDataType.abilityData[1];
            const hiddenAbility = detailPokemonAllDataType.abilityData[2];

            const firstAbilityName = firstAbility ? standardizeName(firstAbility.name) : null;
            const secondAbilityName = secondAbility ? standardizeName(secondAbility.name) : null;
            const hiddenAbilityName = hiddenAbility ? standardizeName(hiddenAbility.name) : null;

            const firstAbilityUrl = firstAbilityName
                ? "https://bulbapedia.bulbagarden.net/wiki/" + firstAbilityName.replace(" ", "_") + "_(Ability)"
                : null;

            const secondAbilityUrl = secondAbilityName
                ? "https://bulbapedia.bulbagarden.net/wiki/" + secondAbilityName.replace(" ", "_") + "_(Ability)"
                : null;

            const hiddenAbilityUrl = hiddenAbilityName
                ? "https://bulbapedia.bulbagarden.net/wiki/" + hiddenAbilityName.replace(" ", "_") + "_(Ability)"
                : null;

            const firstAbilityText = firstAbility
                ? firstAbility.effect_entries.find(e => e.language.name === "en")
                : undefined
            const secondAbilityText = secondAbility
                ? secondAbility.effect_entries.find(e => e.language.name === "en")
                : undefined;
            const hiddenAbilityText = hiddenAbility
                ? hiddenAbility.effect_entries.find(e => e.language.name === "en")
                : undefined;


            const firstAbilityJSX = firstAbility && firstAbilityName && firstAbilityUrl
                ? <Anchor href={firstAbilityUrl} target={"_blank"}>
                    <Badge variant="gradient" h={40} style={{width: "90%"}}
                           gradient={{from: pokemonMainColor, to: pokemonMainColor, deg: 35}}>
                        <Text fz={15}>
                            {firstAbilityName}
                        </Text>
                    </Badge>
                </Anchor>
                : <></>;

            const secondAbilityJSX = secondAbility && secondAbilityName && secondAbilityUrl
                ? <Anchor href={secondAbilityUrl} target={"_blank"}>
                    <Badge variant="gradient" h={40} style={{width: "90%"}}
                           gradient={{from: pokemonMainColor, to: pokemonMainColor, deg: 35}}>
                        <Text fz={15}>
                            {secondAbilityName}
                        </Text>
                    </Badge>
                </Anchor>
                : <></>;

            const hiddenAbilityJSX = hiddenAbility && hiddenAbilityName && hiddenAbilityUrl
                ? <Anchor href={hiddenAbilityUrl} target={"_blank"}>
                    <Badge variant="filled" h={40} style={{width: "90%"}} color={"dark"}>
                        <Text fz={15}>
                            {hiddenAbilityName}
                        </Text>
                    </Badge>
                </Anchor>
                : <></>;


            return (
                <Stack spacing={10}>
                    <SimpleGrid cols={2}>

                        {
                            firstAbility
                                ? <>
                                    <Center>
                                        <Text fz={20} fw={700} c={pokemonMainColor}>
                                            First Ability
                                        </Text>
                                    </Center>

                                    {
                                        firstAbilityText
                                            ? <Tooltip label={firstAbilityText.effect} style={{maxWidth: 450}}
                                                       multiline={true} bg={pokemonMainColor}>
                                                {firstAbilityJSX}
                                            </Tooltip>
                                            : firstAbilityJSX
                                    }

                                </>
                                : <></>
                        }

                        {
                            secondAbility
                                ? <>
                                    <Center>
                                        <Text fz={20} fw={700} c={pokemonMainColor}>
                                            Second Ability
                                        </Text>
                                    </Center>

                                    {
                                        secondAbilityText
                                            ? <Tooltip label={secondAbilityText.effect} style={{maxWidth: 450}}
                                                       multiline={true} bg={pokemonMainColor}>
                                                {secondAbilityJSX}
                                            </Tooltip>
                                            : secondAbilityJSX
                                    }

                                </>
                                : <></>
                        }

                        {
                            hiddenAbility
                                ? <>
                                    <Center>
                                        <Text fz={20} fw={700} c={'dark'}>
                                            Hidden Ability
                                        </Text>
                                    </Center>

                                    {
                                        hiddenAbilityText
                                            ? <Tooltip label={hiddenAbilityText.effect} style={{maxWidth: 450}}
                                                       multiline={true} bg={'dark'}>
                                                {hiddenAbilityJSX}
                                            </Tooltip>
                                            : hiddenAbilityJSX
                                    }

                                </>
                                : <></>
                        }
                    </SimpleGrid>
                </Stack>
            )

        }

        mainContent = (
            <Center className={"detailPokemon-pokemonInfo"}>
                <Stack spacing={20}>
                    <Stack spacing={0}>
                        <Center>
                            <Image className={"detailPokemon-pokemonInfo-pokemonImage"}
                                   src={imageURL}
                                   alt={"pokemon-pic"}
                                   withPlaceholder
                                   placeholder={<Loader variant={"bars"} c={"blue"}/>}/>
                        </Center>
                        <Stack spacing={6}>
                            <Text className={"detailPokemon-pokemonInfo-pokeDexNumText"} ta={"center"}
                                  c={"gray"}>{String(props.selectedPokemonForm.indexPokeDex + 1).padStart(4, "0")}</Text>
                            <Stack spacing={5}>
                                <Text className={"detailPokemon-pokemonInfo-pokemonDisplayName"}
                                      ta="center" fw={700}
                                      c={"black"}>{props.selectedPokemonForm.pokemonDisplayName}</Text>
                                <Stack spacing={12}>
                                    <Text fz={18} ta={"center"} color={"dimmed"}>{pokemonGenusEnglishName}</Text>
                                    {pokeTypeText}
                                </Stack>
                            </Stack>
                        </Stack>
                    </Stack>
                    <div className={"detailPokemon-pokemonInfo-infoArea"}>
                        <Stack spacing={20}>
                            <PokemonInfoGeneralArea
                                leftImage={<GiAlliedStar size={30}
                                                         color={pokemonMainColor}/>}
                                title={"Defensive Type Effectiveness"}
                                isCollapsable={true}
                                defaultCollapsed={true}
                                pokemonFirstTypeName={pokemonFirstTypeName}
                                rightImage={<AiFillCaretDown size={30}
                                                             color={pokemonMainColor}/>}>
                                {getPokemonTypeEffectiveAreaJSX()}
                            </PokemonInfoGeneralArea>

                            <PokemonInfoGeneralArea
                                leftImage={<GiAlliedStar size={30}
                                                         color={pokemonMainColor}/>}
                                title={"Base Stats"}
                                isCollapsable={true}
                                defaultCollapsed={true}
                                pokemonFirstTypeName={pokemonFirstTypeName}
                                rightImage={<AiFillCaretDown size={30}
                                                             color={pokemonMainColor}/>}>
                                {getPokemonBaseStatJSX()}
                            </PokemonInfoGeneralArea>
                        </Stack>
                        <Stack spacing={20}>

                            <PokemonInfoGeneralArea
                                leftImage={<GiAlliedStar size={30}
                                                         color={pokemonMainColor}/>}
                                title={"Ability Data"}
                                isCollapsable={true}
                                defaultCollapsed={true}
                                pokemonFirstTypeName={pokemonFirstTypeName}
                                rightImage={<AiFillCaretDown size={30}
                                                             color={pokemonMainColor}/>}>
                                {getPokemonAbilityJSX()}
                            </PokemonInfoGeneralArea>

                            {
                                moveListMap.size > 0
                                    ? (
                                        <PokemonInfoGeneralArea
                                            leftImage={<GiAlliedStar size={30}
                                                                     color={pokemonMainColor}/>}
                                            title={"Move List"}
                                            isCollapsable={true}
                                            defaultCollapsed={false}
                                            pokemonFirstTypeName={pokemonFirstTypeName}
                                            rightImage={<AiFillCaretDown size={30}
                                                                         color={pokemonMainColor}/>}>
                                            {getPokemonMoveListJSX()}
                                        </PokemonInfoGeneralArea>
                                    )
                                    : <></>
                            }

                        </Stack>
                    </div>
                </Stack>
            </Center>
        );

    }
    return (
        <div className={"detailPokemon"}>
            <ActionIcon size={40} style={{zIndex: 1000}} onClick={() => {
                props.handleBackButtonClick();
            }}>
                <TiArrowBack color={"black"} size={40} className={"detailPokemon-backBtn"}/>
            </ActionIcon>

            <ActionIcon className={"detailPokemon-fullScreenIcon"}
                        onClick={() => props.setFullScreenMode(!props.fullScreenMode)}>
                <Image width={30}
                       src={"images/fullscreen-icon.svg"}
                       alt="fullscreen-icon"/>
            </ActionIcon>
            <div className={"detailPokemon-pokeballPicture"}>
                <PokeBallLogo/>
            </div>
            {
                pokemonLoaded ? mainContent : loaderJSX
            }

        </div>
    )
        ;
}

interface PokemonInfoGeneralAreaProps {
    leftImage: JSX.Element,
    title: string,
    isCollapsable: boolean,
    rightImage: JSX.Element,
    defaultCollapsed: boolean,
    children: JSX.Element[] | JSX.Element,
    pokemonFirstTypeName: string
}

// @ts-ignore
function PokemonInfoGeneralArea(props: PokemonInfoGeneralAreaProps) {
    const [collapsed, {toggle}] = useDisclosure(props.defaultCollapsed);

    const handleClick = () => {
        if (props.isCollapsable)
            toggle();
    }

    const childrenJSX = (
        <Stack spacing={10} style={{padding: "10px 10px"}}>
            {props.children}
        </Stack>
    )

    return (
        <Stack spacing={0} className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea"}>
            <Group position={"apart"} style={{
                borderRadius: collapsed ? "16px 16px 0 0" : 16,
                backgroundColor: props.pokemonFirstTypeName === "dragon" ? "var(--accent-color-4)" : "var(--accent-color-8)"
            }}
                   className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-titleBar"}>
                <Group spacing={10}>
                    {props.leftImage}
                    <Text
                        className={"detailPokemon-pokemonInfo-infoArea-pokemonInfoGeneralArea-titleBar-title"}>{props.title}</Text>
                </Group>
                <Center onClick={handleClick} style={{
                    rotate: (collapsed || !(props.isCollapsable)) ? "" : "-180deg",
                    transition: '0.3s ease-in-out'
                }}>
                    {props.rightImage}
                </Center>
            </Group>

            {props.isCollapsable
                ? (
                    <Collapse in={collapsed} transitionDuration={300}>
                        {childrenJSX}
                    </Collapse>
                )
                : childrenJSX}

        </Stack>
    );
}
