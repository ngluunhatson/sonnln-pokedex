import React, {useEffect, useState} from "react";
import {ActionIcon, Button, Checkbox, Group, Image, SimpleGrid, Stack, Text, TextInput, Tooltip} from "@mantine/core";
import {MdCancel, MdSort} from "react-icons/md";
import {pokemonTypeColorCode, typeNameArray} from "@/constants/programConstants";
import {standardizeName} from "@/constants/programFunctions";
import {AiOutlineClose} from "react-icons/ai";

interface ListPokemonSortFilterProps {
    setStringToFilter: (value: (((prevState: string) => string) | string)) => void,
    setTypeStringArray: (value: (((prevState: string[]) => string[]) | string[])) => void,
    typeStringArray: string[]
}

function ListPokemonSortFilter(props: ListPokemonSortFilterProps) {
    const [showTypeSort, setShowTypeSort] = useState(false);
    const [stringToFilterSort, setStringToFilterSort] = useState("");
    const [typeStringArraySort, setTypeStringArraySort] = useState<string[]>([]);
    useEffect(() => {
        if (stringToFilterSort.length === 0) {
            props.setStringToFilter("");
        }
    }, [stringToFilterSort]);


    return (
        <Stack spacing={15} className={"listPokemon-control"}>
            <Group position={"apart"} style={{position: "relative"}}>
                <ActionIcon onClick={() => setShowTypeSort(!showTypeSort)}>
                    <MdSort className={"listPokemon-control-sortButton"} size={25}/>
                </ActionIcon>
                {showTypeSort
                    ? (
                        <Stack className={"listPokemon-control-typeSort"} spacing={12}>
                            <Group spacing={0} position={"apart"}>
                                <div style={{width: "10%"}}></div>
                                <Text ta={"center"} className={"listPokemon-control-typeSort-title"}>Filter by
                                    Type</Text>
                                <ActionIcon size={25} onClick={() => setShowTypeSort(false)}>
                                    <AiOutlineClose color={"black"}/>
                                </ActionIcon>

                            </Group>

                            <Checkbox.Group value={typeStringArraySort} onChange={setTypeStringArraySort}>
                                <SimpleGrid cols={4}>
                                    {typeNameArray.map((typeName, index) => {
                                        return (
                                            <div className={"listPokemon-control-typeSort-checkBox"}
                                                 key={`type-check-box-${index}`}>
                                                <Checkbox
                                                    value={typeName}
                                                    label={
                                                        <Tooltip
                                                            color={pokemonTypeColorCode[typeName]}
                                                            label={standardizeName(typeName)}>
                                                            <Image src={`/images/type_icons/${typeName}.svg`} alt={""}
                                                                   width={20}/>
                                                        </Tooltip>}
                                                />
                                            </div>
                                        );
                                    })}

                                </SimpleGrid>
                            </Checkbox.Group>
                            <Group position={"center"}>
                                <Button className={"listPokemon-control-typeSort-button"}
                                        onClick={() => {
                                            if (typeStringArraySort.length > 0 || (typeStringArraySort.length === 0 && props.typeStringArray.length !== 0))
                                                props.setTypeStringArray(typeStringArraySort)
                                        }}>
                                    Filter
                                </Button>
                                {
                                    typeStringArraySort.length > 0
                                        ? <Button bg={"red"}
                                                  onClick={() => {
                                                      if (props.typeStringArray.length > 0) {
                                                          props.setTypeStringArray([]);
                                                      }
                                                      setTypeStringArraySort([]);
                                                  }}>
                                            Clear
                                        </Button>
                                        : <></>
                                }
                            </Group>

                        </Stack>
                    )
                    : <></>}


                <Group position={"center"}>
                    <Text className={"listPokemon-control-text"} fw={"700"} color="white"
                          ta={"center"}>The Ultimate Pokédex</Text>
                </Group>
                <div></div>
            </Group>
            <Group position={"apart"} spacing={2}>
                <TextInput
                    value={stringToFilterSort}
                    rightSection={stringToFilterSort.length === 0
                        ? <></>
                        : <MdCancel color={"gray"} onClick={() => setStringToFilterSort("")}/>}
                    style={{width: "75%"}}
                    onChange={e => {
                        setStringToFilterSort(e.target.value)
                    }}/>
                <Button className={"listPokemon-control-searchButton"}
                        onClick={() => props.setStringToFilter(stringToFilterSort)}>Search</Button>
            </Group>

        </Stack>
    );
}

export default ListPokemonSortFilter;
