import React, {useState} from "react";
import {Group, Image, Stack, Text} from "@mantine/core";
import {controlPanelItems} from "@/constants/programConstants";

interface ControlPanelProps {
    handleClick: (value: number) => void,
    currentIndex: number
}

function ControlPanel(props: ControlPanelProps) {
    const [selectedIndex, setSelectedIndex] = useState(0);
    const handleButtonClick = (index: number) => {
        setSelectedIndex(index);
        props.handleClick(index);
    }

    return (
        <Group position={"apart"} className={"controlPanel"}
               style={{padding: `0px ${100 / (controlPanelItems.length - 1)}px`}}>
            {controlPanelItems.map((controlPanelItem, index) =>
                <ControlPanelItemButton imageURL={controlPanelItem.imageURL}
                                        isSelected={props.currentIndex !== 0 ? selectedIndex === index : index === 0}
                                        handleClick={handleButtonClick} index={index}
                                        title={controlPanelItem.title} key={`controlPanelItem-${index}`}/>)
            }
        </Group>
    );
}

export default ControlPanel;


interface ControlPanelItemButtonProps {
    isSelected: boolean,
    imageURL: string,
    title: string,
    index: number,
    handleClick: (index: number) => void
}

function ControlPanelItemButton(props: ControlPanelItemButtonProps) {
    const [hovered, setHovered] = useState(false);
    const handelClick = (index: number) => {
        props.handleClick(index);
    }

    return (
        <Group position={"center"} onClick={() => handelClick(props.index)} onMouseEnter={() => setHovered(true)}
               onMouseLeave={() => setHovered(false)}>
            <Stack spacing={1} align={"flex-start"}>
                <div className={"controlPanel-focusBar "
                    + (props.isSelected ? "controlPanel-focusBar-selected" : "")
                    + (hovered && !props.isSelected ? "controlPanel-focusBar-hovered" : "")}></div>
                <Stack spacing={1}>
                    <Group position={"center"} w={45}>
                        <Image width={25} src={props.imageURL}
                               alt={props.title}/>
                    </Group>
                    <Text ta="center" className={"controlPanel-text"}>{props.title}</Text>
                </Stack>
            </Stack>
        </Group>
    );
}
