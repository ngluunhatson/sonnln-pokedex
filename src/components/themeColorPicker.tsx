import React, {useEffect, useState} from 'react'
import {ActionIcon, Center, NativeSelect, Stack, Text} from "@mantine/core";
import {accentColorLocalStorageKey} from "@/constants/localStorageKeyConstants";
import {TiArrowBack} from "react-icons/ti";

interface ThemeColorPickerProps {
    handleBackButtonClick: () => void
}

function ThemeColorPicker(props: ThemeColorPickerProps) {
    const [activeAccentColor, setActiveAccentColor] = useState('');

    useEffect(() => {

        const accentColorLocalStorage = window.localStorage.getItem(accentColorLocalStorageKey);
        if (accentColorLocalStorage) {
            setActiveAccentColor(accentColorLocalStorage);
        }
    }, []);


    useEffect(() => {
        window.localStorage.setItem(accentColorLocalStorageKey, activeAccentColor);
    }, [activeAccentColor]);

    useEffect(() => {
        const root = document.documentElement;
        for (let i = 0; i <= 9; i++) {
            root.style.setProperty(`--accent-color-${i}`, `var(--mantine-color-${activeAccentColor}-${i})`);
        }
        root.style.setProperty('--background-color', `var(--mantine-color-${activeAccentColor}-0)`);
    }, [activeAccentColor]);

    return (
        <Stack spacing={20} style={{height: "100dvh"}}>
            <ActionIcon size={40} onClick={() => {
                props.handleBackButtonClick();
            }}>
                <TiArrowBack size={40} color={"black"}/>
            </ActionIcon>

            <Center style={{height: "50%"}}>
                <Stack w={500} spacing={30}>
                    <Text fw={700} fz={25}>General Settings</Text>
                    <NativeSelect
                        label={"Theme Color"}
                        data={['red', 'pink', 'grape', 'violet', 'indigo', 'blue', 'cyan', 'teal', 'green', 'lime', 'yellow', 'orange']}
                        value={activeAccentColor}
                        onChange={(e) => setActiveAccentColor(e.currentTarget.value)}/>
                </Stack>
            </Center>


        </Stack>
    )
}

export default ThemeColorPicker;
