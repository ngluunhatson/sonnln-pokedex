import type {AppProps} from "next/app";
import {MantineProvider} from "@mantine/core";
import React, {useEffect} from "react";
import {accentColorLocalStorageKey} from "@/constants/localStorageKeyConstants";
import Head from "next/head";
import "@/scss/index.scss";
import { Analytics } from '@vercel/analytics/react';

export default function App({Component, pageProps}: AppProps) {
    useEffect(() => {
        const root = document.documentElement;
        const accentColorLocalStorage = window.localStorage.getItem(accentColorLocalStorageKey);
        let accentColor = "blue";
        if (accentColorLocalStorage) {
            accentColor = accentColorLocalStorage;
        }
        for (let i = 0; i <= 9; i++) {
            root.style.setProperty(`--accent-color-${i}`, `var(--mantine-color-${accentColor}-${i})`);
        }
        root.style.setProperty("--background-color", `var(--mantine-color-${accentColor}-0)`);
    }, []);
    return (
        <>
            <MantineProvider withCSSVariables withGlobalStyles withNormalizeCSS>
                <Head>
                    <title>The Ultimate Pokédex</title>
                    <meta name="description" content="Son Nguyen Fullstack Developer Portfolio"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                    <link rel="icon" type="image/png" href="/images/pokeball-favicon.png"/>
                </Head>
                <Component {...pageProps} />
            </MantineProvider>
            <Analytics/>
        </>
    );
}
