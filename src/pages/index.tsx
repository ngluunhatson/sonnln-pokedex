import React, {useState} from "react";
import ListPokemon from "@/components/listPokemon";
import {Center, Group, Progress, Stack, Text} from "@mantine/core";
import ControlPanel from "@/components/controlPanel";
import ThemeColorPicker from "@/components/themeColorPicker";
import DetailPokemon from "@/components/detailPokemon";
import {SelectedPokemonForm} from "@/constants/programConstants";
import {Simulate} from "react-dom/test-utils";


export default function Index() {
    const initialSelectedPokemonFormState = {
        indexPokeDex: -1,
        indexPokeDexChild: -1,
        pokeForm: null,
        pokemonDisplayName: "",
    };

    const [progressBarPercent, setProgressBarPercent] = useState(0);
    const [contentShownIndex, setContentShownIndex] = useState(0);
    const [selectedPokemonForm, setSelectedPokemonForm]
        = useState<SelectedPokemonForm>(initialSelectedPokemonFormState);
    const [fullScreenMode, setFullScreenMode] = useState(false);
    const [isMainControllerHidden, setIsMainControllerHidden] = useState(false);


    const handleControlPanelClick = (index: number) => {
        setContentShownIndex(index);
        setIsMainControllerHidden(!(index === 0));
    };

    const handleBack = () => {
        setContentShownIndex(0);
        setIsMainControllerHidden(false);
    };

    const handleListPokemonClick = (selectedPokemonForm: SelectedPokemonForm) => {
        setSelectedPokemonForm(selectedPokemonForm);
        setContentShownIndex(0);
        setIsMainControllerHidden(true);
    };

    let contentShown = <></>;

    switch (contentShownIndex) {
        case 0:
            contentShown = <DetailPokemon fullScreenMode={fullScreenMode} setFullScreenMode={setFullScreenMode}
                                          selectedPokemonForm={selectedPokemonForm}
                                          handleBackButtonClick={handleBack}/>;
            break;
        case 1:
            contentShown = <ThemeColorPicker handleBackButtonClick={handleBack}/>;
            break;
        default:
            break;
    }

    return (
        <>
            <Group
                className={"pokeDex " + (fullScreenMode ? " fullScreenMode" : "") + (progressBarPercent >= 100 ? "" : "loadingNotDone")}>
                <Stack className={"pokeDex-mainController" + (isMainControllerHidden ? " hiddenContent" : "")}>
                    <ListPokemon handleListPokemonClick={handleListPokemonClick}
                                 setProgressBarPercent={setProgressBarPercent}
                                 selectedPokemonForm={selectedPokemonForm}/>
                    <ControlPanel currentIndex={contentShownIndex} handleClick={handleControlPanelClick}/>
                </Stack>
                <div className={"pokeDex-contentShown" + (!isMainControllerHidden ? " hiddenContent" : "")}>
                    {contentShown}
                </div>
            </Group>

            {
                progressBarPercent < 100
                    ? (<Center>
                            <Stack spacing={2} justify={"center"} w={200} style={{
                                height: "100vh"
                            }}>
                                <Text fw={700} fz={12}>Loading list...</Text>
                                <Progress className={"progressBar"} value={progressBarPercent} animate/>
                            </Stack>
                        </Center>
                    )
                    : <></>

            }


        </>
    );
}


